---
layout: page
title: "Documentation"
---

## Using Passit

- [LDAP Integration](ldap/)

## Hosting Passit on Your Own Server

- [Installation Guide](/install/)
- [Update Guide](/update/)

## Contributing to Passit

- [Contributing Overview](contributing/)
- [Code on GitLab](https://gitlab.com/passit/)

## Technical Specs

- [Security Model Wiki](https://gitlab.com/passit/passit/wikis/security)
- [Security Model Diagrams](security-model/)
