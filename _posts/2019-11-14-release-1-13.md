---
layout: post
title:  "Passit 1.13: A better group form"
author: Brendan Berkley
date:   2019-11-14 00:00:00 -0400
categories: release
---

Passit 1.13 has been released. It features a redesign of the group form to allow for easier selection of group members.

Give it a try! Click "Groups" up top and either add or edit a group. There's a lot more space to see and interact with users in your group, and most of the odd and confusing aspects of user management have been fixed.

If you've never made a group, now's a good time to start! You can use them to keep yourself organized, or you can invite coworkers, family, or friends to share passwords together.

### What's next

We will continue work on two-factor authentication that has been happening behind the scenes. We also plan on adapting our group form work to the password form to improve group selection and management. Some app improvements are in the pipeline as well, so keep an eye out for group form improvements there, as well as the ability to download a new backup code.

Don't forget to add a thumbs up to your most requested features in [GitLab](https://gitlab.com/groups/passit/-/issues)!
