---
layout: post
title:  "Threat Talk: &ldquo;There is already an account associated with this email address&rdquo;"
date:   2017-10-27 17:39:00 -0500
categories: threat-talk
excerpt: >
    If an attacker can see that your email address is used on Passit, is that bad?
    Should we prevent the threat? How can you protect yourself?
---

Security is a balancing act of competing priorities. It has to be weighed against other factors such as convenience, privacy, and cost. When designing and building Passit, we have to think about these tradeoffs all the time. This post documents a threat that we analyzed pretty thoroughly and then made an intentional decision about.

### What's the threat?

An attacker is able to determine if a particular email account is already registered on the site.

We'll discuss this in [Alice and Bob](https://en.wikipedia.org/wiki/Alice_and_Bob) terms from here. If Alice's username is alice@example.com, Chuck can determine that this email address is registered with Passit.

### Why is this is a threat?

Chuck could be indirectly targeting Alice. Maybe he got her email address from a data breach, where Alice's email and password from another site was hacked. He'll run every email address and password on the list. If Alice has the same email and password for Passit, she's out of luck (which is why your Passit password should be unique!). If Alice has the same email but a different password, then Chuck will at least know that Alice is registered with Passit.

Chuck could be specifically targeting Alice as well, which equals a similar threat. If he enters her email address and sees a positive match, then he knows that it'll be worth the effort to try and get her password. 

The password will always be Alice's most important defense. However, if our system never revealed that Alice's email is registered, then Chuck will never know for sure if his brute-forcing efforts are being directed at a real account.

### Why don't you stop the threat?

We talked through this for awhile. If we could completely obscure email addresses, then Chuck would never know if his list of accounts was hitting real accounts in Passit, which is a real security gain. We could also add usernames so we wouldn't have to use emails as identifiers. However:

1. Usernames of any kind aren't secrets. They're not encrypted on the server and they're used in public features like sharing, so they shouldn't be treated the same as passwords.
2. Email addresses are generally regarded as better identifiers than a username that the user picks. This is stated with user experience and support considerations in mind. My email address is unique, so I don't need to spend time thinking of a username that isn't taken, nor do I need to settle for brendan13574. I'm also more likely to remember my email address, and we don't need to worry about a username recovery service. Plus, when it comes to sharing, it's a lot easier to make those connections with an email.
3. It's not common for password managers to completely obscure email addresses.
4. While we could obscure email addresses from most people without too much effort, we decided that preventing a [timing attack](https://en.wikipedia.org/wiki/Timing_attack) would be a difficult and time-consuming task, and because of the three points above we didn't think the effort was necessary.
5. In the absence of a total solution, a partial solution is not ideal. Offering only a partial solution would only provide a false sense of security while taking convenience away from the user.

### How do you protect me against this threat?

If Chuck is specifically going after Alice, there's not a lot we can do. Chuck puts in Alice's email address and it's either there or it isn't.

However, if Chuck has a list of 100,000 email addresses to test and Alice is on that list, we can help. We use rate limiting to help protect against this kind of attack.

### How do I protect myself against this threat?

It's up to you to decide if this is a big deal to you or not. All of the major password managers that I researched will reveal if you have an account, so this is a pretty common thing.

However, there are a few ways you could get around this problem on your own:

1. Create a separate email address exclusively for Passit. This introduces all of the user experience problems of a username (namely, you'd have to memorize this or write it down somewhere), but would have the same functional effect.
2. If your email supports it, use the `+` sign. `alice+passit@example.com`, or `alice+1hg8jk309x874ubho3zr@example.com`. Similar issues as 1, but it goes to the same email account at least.
3. Host Passit on your own server and use whatever email addresses you want. As long as it has an `@` and two characters after a `.`, you can use it. I use accounts like `a@a.aa` all the time when working on the site locally.

### What parts of Passit does this affect?

Login, registration, and sharing with other users.
