---
layout: post
title: "A Preface to Passit, Part 3: Why use an open source app?"
author: Brendan Berkley
excerpt: >
    We freely let people see, use, and modify the source code that runs Passit.
    Why is that a good idea, and why should you care enough to make a decision
    based on it?
date: 2018-03-28 00:00:00 -0400
categories: ideas
---

*This is the third installment of a five-part series. Links to the other posts can be found at the end of this&nbsp;article.*

For the purposes of this post, open source means that anyone, including you, can download and view the code that makes Passit work. You can run Passit on your own computer without ever paying us a dime. You're even free to make changes to Passit and distribute your own version of it (though you'd need a new brand name and logo to do the latter).

If you're not familiar with open source, your first reaction is probably: "What? That's crazy!" Why would we give our product away for free? Why would we allow competitors to rip off our hard work?

There are a lot of reasons to do open source, both practical and ideological. The biggest reason why it matters in the security world is a practical one: it allows anyone to view the code and verify that the software is doing what we claim it's doing. This is really important.

If you remember the [FBI's dispute with Apple in 2016](https://en.wikipedia.org/wiki/FBI%E2%80%93Apple_encryption_dispute), the FBI tried to compel Apple to make software that circumvented their own security. This dispute happened publicly, and resolved in a way that Apple didn't have to do this. However, it reminded us that closed source software is very dependent on trust in the people and companies that make it. It's not a reach to believe that user-hostile changes to closed-source code can be made without the average user ever discovering something was different.

If you think that's tinfoil hat territory, then consider a more common case: a closed-source system can introduce a flaw that goes unnoticed because no one can see the code. If the source is viewable, members of the community can audit the changes, spot problems, and report them effectively. If you're hosting your own instance of Passit, you can examine our [release notes]({{ site.baseurl }}/blog/category/release/) and [code changes](https://gitlab.com/groups/passit/-/activity) to decide if and when you want to update, or you can modify the code to add features that you need for your purposes.

Also, consider some of the privacy concerns with companies like Facebook or Google, particularly in light of the [Cambridge Analytica controversy](https://en.wikipedia.org/wiki/Facebook#Cambridge_Analytica). These companies provide excellent services, but can often leave you wondering how they use your data and how they are making money off of you. You'll almost never hear those concerns coming from people who use Firefox, Ubuntu, or WordPress, because even if you're not savvy enough to evaluate their codebases, other people are, and you'll find out quickly if they're attempting something you wouldn't approve of.

In short, we give up some control and some profit potential to gain your trust and give you more freedom. That's a win for everyone.

###A Preface to Passit:

- [Part 1: Liberty vs. safety in an imperfect world]({% post_url 2018-03-05-preface-to-passit-1-liberty-vs-safety %})
- [Part 2: Why use a password manager?]({% post_url 2018-03-15-preface-to-passit-2-why-a-password-manager %})
- Part 3: Why use an open source app?
- [Part 4: Why use a cloud-based password manager?]({% post_url 2018-04-05-preface-to-passit-4-why-cloud-based %})
- [Part 5: Why pick Passit?]({% post_url 2018-04-18-preface-to-passit-5-why-passit %})
