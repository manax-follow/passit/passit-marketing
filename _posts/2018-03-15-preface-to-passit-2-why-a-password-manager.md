---
layout: post
title: "A Preface to Passit, Part 2: Why use a password manager?"
author: Brendan Berkley
excerpt: >
    Password managers take a bit of work to use, and most people don't make the
    effort. Why should you take the extra steps to make your secrets safe?
date: 2018-03-15 00:00:00 -0400
categories: ideas
---

*This is the second installment of a five-part series. Links to the other posts can be found at the end of this&nbsp;article.*

The folks at RoboForm [did a survey](https://www.roboform.com/blog/password-security-survey-results) about how people handle passwords and Internet security. They found that only 8% of their respondents use a password manager like RoboForm or Passit, but the reality is that anyone who uses the Internet has a system to manage passwords. The survey indicates as such: 74% of you will sometimes let your browser do the work for you, 42% of you write down your passwords, and 10% of you will save some passwords in a document. Some of you pick something easy to remember and use it everywhere, or use variants on the same password across multiple sites.

Anyone who follows Internet security best practices will tell you that just about all of those things are bad. But hey, why should you care? I mean this sincerely. You've probably never been hacked, and if you have, it probably just ended up being something dumb like your Facebook friends getting a bunch of spam from you. Alternatively, your sheet of paper in your desk drawer and/or Google doc on the company account works well enough. Maybe you feel a little weird about it, but it generally does the job. Identity theft, foreign hackers, government spying...you see it in the news, but there are 7 billion people in the world, and no one cares about me.

We get it. Seriously. All of the excuses bely some simple truths: passwords are annoying, and we internalize neither the threats on the Internet that a good password manager can mitigate nor the benefits that a good password manager can offer to make this stuff easier and better. Plus, people like us sound like other people you know who always make you feel terrible about your life decisions. Investment managers will tell you how important your retirement fund is. Dentists will tell you how important getting that regular cleaning is. Lawyers will chide you for falling behind on your will. Accountants will lament your bookkeeping skills. Dieticians will wonder why you can't just stick to the weight loss plan. And yet, you still manage to get out of bed in the morning and live your life well enough.

So...why a password manager? Why listen to us?

Password managers are probably the best mix of freedom and safety. Going back to the  "door lock as a password" metaphor, locks are a relatively cheap and convenient way to make your place way more secure than a residence that has a door without a lock. They aren't the only way to secure your home, though. You could hire a security guard to stand at the entrance. You could place land mines in spots that you can avoid when approaching the entrance (subject to your local municipality's laws, of course). You could place the entrance on the second floor and provide a single rope as the only way to get up. But most of these solutions are dumb, expensive, or restrictive. If you must keep unwelcome guests out of your home, locks are a great mix of cost, unobtrusiveness, and user-friendliness, and they bring some new benefits along the way such as a greater feeling of privacy.

In the same way, password managers have some issues of their own, but they're a pretty easy way to beef up the security and convenience in your digital life. You're probably storing passwords somehow anyways; store them in a spot that was created to store them. You might be (should be!) trying to remember multiple passwords; put them in one spot so you only have to remember the password that logs you into the manager. Now that you only need to remember one password, make your passwords longer and tougher to hack.

Using a password manager can give you extra security with minimal effort, and peace of mind knowing that your passwords are safe somewhere and you don't need to keep them in your brain. Sounds like a win to us!

### A Preface to Passit:

- [Part 1: Liberty vs. safety in an imperfect world]({% post_url 2018-03-05-preface-to-passit-1-liberty-vs-safety %})
- Part 2: Why use a password manager?
- [Part 3: Why use an open source app?]({% post_url 2018-03-28-preface-to-passit-3-why-open-source %})
- [Part 4: Why use a cloud-based password manager?]({% post_url 2018-04-05-preface-to-passit-4-why-cloud-based %})
- [Part 5: Why pick Passit?]({% post_url 2018-04-18-preface-to-passit-5-why-passit %})
