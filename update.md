---
layout: page
title: "Passit Update Guide"
---

*To learn more about installing Passit, visit our [installation guide](/install/).*

Upgrading consists of getting the latest Docker images, deploying them, and then running database migrations, which will happen manually or automatically depending on where you're working.

# Docker

To upgrade using Docker Compose:

1. Get the latest docker image `docker-compose pull`
2. Shut down the server `docker-compose down`
3. Migrate the database `docker-compose run --rm app ./manage.py migrate`
4. Start the database `docker-compose up -d`

# OpenShift

Update the Passit Docker image stream. This will trigger a deploy. You can do this by pulling the Docker image, tagging it, and pushing it to OpenShift.

Make sure you are logged into the Openshift registry. Read [here](https://docs.openshift.com/online/dev_guide/managing_images.html#accessing-the-internal-registry).

1. `docker pull {{site.docker_image}}`
2. `docker tag {{site.docker_image}} <your-openshift-registry>/<openshift-project-name>/passit:latest`
3. `docker push <your-openshift-registry>/<openshift-project-name>/passit:latest`

If you used our openshift [template](/passit-openshift/), your database will automatically update using a pre-deploy hook.

Your registry will change depending on where you deployed Openshift to. If you used Openshift Online in US East 1 it would be `registry.pro-us-east-1.openshift.com`

# Heroku

Update the Passit Docker image. You can do this by pulling the Docker image, tagging it, and pushing it to Heroku.

Make sure you are logged into the Heroku registry. Read [here](https://devcenter.heroku.com/articles/container-registry-and-runtime#logging-in-to-the-registry).

1. `docker pull {{site.docker_image}}`
2. `docker tag {{site.docker_image}} registry.heroku.com/<heroku-app-name>/web`
3. `docker push registry.heroku.com/<heroku-app-name>/web`
4. `heroku container:release web -a <heroku-app-name>`
6.  Manually migrate the database using a one-off dyno. `heroku run ./manage.py migrate -a <heroku-app-name>`

# Without Docker

This is not supported. However, if you pull the latest code from the passit-frontend and passit-backend repos and then run the database migration, it should work.
