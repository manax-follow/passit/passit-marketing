---
layout: post
title:  "Building Passit's marketing site"
date:   2016-03-24 00:00:00 -0400
categories: development
author: Brendan Berkley
---

As the front-end guy at Burke Software and Consulting as well as the occasional graphic designer, I generally don't get the chance to make architecture decisions. And that's generally fine&mdash;DevOps, architecture, backends, these aren't really my thing. But since Passit's marketing site is going to be mostly my domain and because no one else had any real technical needs other than 'edit easily', I wanted to make sure that I got what I wanted.

## Enter Jekyll

I don't know what people say about Jekyll these days. I was ready to type a paragraph about how it's not cool anymore and that's fine, but that may be a straw man. I don't know. Here's what I do know:

- It's popular. GitHub Pages has a lot to do with that.
- It's active. Version 3 was released late in 2015 and updates continue to flow.
- It's static. I'm a fan of making sites without a database if it doesn't truly need one. I love WordPress, but sometimes when I make a site it just feels like feature overkill.
- It's capable. Lets me do template tags and partials, lets me use Sass, YAML, and Markdown if I want.
- It's mature.
- It's well-documented. The [official docs](http://jekyllrb.com/) are useful, and [Liquid](http://cheat.markdunkley.com/) [documentation](https://github.com/shopify/liquid/wiki/Liquid-for-Designers) is readily avalable as well.
- It's search-engine-friendly. That is, I can use my favorite search engine to fill in the gaps pretty easily.
- It's Ruby, but not really. If you can 1) copy and paste into a command line, installing Ruby and Jekyll are pretty simple. From there, it's just a matter of typing `jekyll serve` and letting the gem do it's thing, and you can focus on the front end.
- It's familiar to me. You don't care about that of course, but it's a useful thing to consider. I've spent a lot of time in Jekyll for a large prototyping project, and I got to work with it quite a bit that way. I learned how to deploy to GitHub Pages as well as Heroku, and I even got a bit of experience with writing plugins and making it work with Docker.

## Bootstrapping Jekyll

Jekyll's installed. What of the Sass? I had two choices for myself: 

1. Go custom. Like, 100% custom. The thinking here is that a framework could be overkill for a basic site, and I could benefit from having a playground to try things out.
2. Bootstrap. Unlike many others (and I know this isn't a straw man to say 'many others'), I stick up for Bootstrap. It gets you started quickly, and if you get into the Sass partials, you can customize things quite a bit or disable things you won't use.

After talking it through with the team, I decided that Bootstrap was best for now. Not only because I like Bootstrap (many reasons I gave for Jekyll apply to Bootstrap as well), but because we wanted to get something up quickly, and because pretty much everything about Passit is at the rough draft level anyways. If we had a logo, color palette, style guide, and software UI all worked out, maybe that would lead us to a more interesting marketing site design that would require more custom work. But if this is a version 1 that just needs to convey info, then...done is better than perfect; Bootstrap it is.

## GitLab Pages / CI

Next question: where to host? When it comes to stock Jekyll, [GitHub Pages](https://pages.github.com/) is the typical starting point. It's popular, proven, and simple to get going. But at Burke Software we've been using [GitLab](gitlab.com) more and more, and we'd planned on hosting the repo there. Heroku was my next choice since I've launched a couple of Jekyll sites on it before. But then David pointed out [GitLab Pages](http://doc.gitlab.com/ee/pages/README.html) and said that since we're hosting on GitLab, might as well host the site there too if we can.

So, it was time for me to learn a new thing! This wasn't as straightforward as pushing a branch in the manner of GitHub Pages. I struggled for awhile, and after referencing [this YAML file](https://gitlab.com/steko/test/blob/88402e1f849c2b7a7af98291135bfeff56811734/.gitlab-ci.yml) and reading around and apparently blatantly missing the [example in the docs](http://doc.gitlab.com/ee/pages/README.html#how-.gitlab-ci.yml-looks-like-when-using-a-static-generator), I got something working. All you need to do is add a `.gitab-ci.yml` file into the root directory of the repo that looks like this:

```
image: ruby:2.1

pages:
  script:
  - gem install jekyll
  - jekyll build -d public/
  artifacts:
    paths:
    - public
  only:
  - master
```

And...success! We were up and running. I got some help from the team with getting the domain name set up, and then we were off and running. [passit.io](http://passit.io) is now live!
