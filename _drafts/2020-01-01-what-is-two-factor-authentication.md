---
layout: post
title:  "What is Two-Factor Authentication?"
author: Brendan Berkley
date:   2020-01-01 00:00:00 -0400
categories: ideas
excerpt: >
    Take some time to set up two-factor authentication, and reap the reward
    of a much more secure Passit account.
---

Have you ever withdrawn money from an ATM by inserting a card and then entering a PIN? If so, then you've done two-factor authentication (2FA) before. An ATM requires something you know (the PIN) and something you have (your card) to withdraw money. Passit requires something you know (your password) for all users, and also allows you to require something you have (an authenticator app on your phone or computer) to make your account more secure.

If that sounds like something you want, go to your account page and then press "Manage two-factor authentication". If your account is at app.passit.io, here's a [direct link][2].

### A longer explanation

At the heart of all authentication is the need to verify that you are who you say you are. On the Internet, most sites require you to enter a password, which only you should know. Passwords can be stolen or cracked though, and while you can avoid this by [picking a good password][0], you can't eliminate this threat entirely.

Therefore, requiring more information to authenticate you is a good thing. Some sites will do this with security questions, but these are essentially just extra passwords, with similar weaknesses. Other sites will do this by sending you a code via text message, but that code [can be intercepted][1].

A better way to require a second form of authentication is to use what's called a time-based one-time password (TOTP) via an authenticator app on your phone (or computer, but most people use phones). By requiring your phone, this fulfills the same "something you know, something you have" pattern that you use at the ATM.

You might have questions! Let's see if we can answer them.

<a name="authenticator-information"><!-- Used in the Manage 2FA page in Passit app --></a>
### What's an authenticator app? Which ones do you recommend?

Popular authenticator apps are:

- [Authy][3]
- [Google Authenticator][4]
- [Windows Authenticator][5]
- [Authenticator for iOS][6]

An authenticator app will look something like this:

<div style="width: 250px; margin: 0 auto;">
	<img src="/images/blog/authenticator.png" alt="Screenshot of authenticator app" />
</div>

When you log in to Passit and it asks for a code, you'll open your app, find a six-digit code for your Passit account, and enter that in to finalize your login. As long as you have your phone nearby while logging in, it doesn't add a huge burden to the login process, and it gives your account's security a massive boost.

### How do I set up two-factor authentication for Passit?

Go to your account page and then press "Manage two-factor authentication" ([direct link][2] for app.passit.io users). We'll walk you through the process, step by step.

### What if I lose the device that has my authenticator app on it?

Passit offers a backup code for account recovery. If you have a backup code downloaded or printed somewhere, you can use it to go through the account recovery process to get back into your account. Once you do that, 2FA will be will be deactivated, so you'll have to go through the setup process again.

If you don't have a backup code and you're still logged in somewhere, you can go to your account page and disable 2FA.

If you don't have a backup code and you are not logged in anywhere, you will be unable to recover your account.

### What if I want to turn two-factor authentication off?

You can do that! In the "Manage two-factor authentication" page, disabling 2FA is a button press away.

Make sure you keep your authenticator app tidy; delete Passit from it after you've disabled 2FA.

### Where can I learn more about how 2FA and TOTP work?

Authy has [a nice introduction][7] to 2FA, and Neil Jenkins at Fastmail did a nice [writeup][8] about how TOTP works.


[0]: https://passit.io/blog/picking-a-good-password/
[1]: https://www.wired.com/2016/06/hey-stop-using-texts-two-factor-authentication/
[2]: https://app.passit.io/account/manage-mfa/
[3]: https://authy.com/download/
[4]: https://play.google.com/store/apps/details?id=com.google.android.apps.authenticator2&hl=en_US
[5]: https://www.microsoft.com/en-us/p/microsoft-authenticator/9nblgggzmcj6?activetab=pivot:overviewtab
[6]: https://mattrubin.me/authenticator/
[7]: https://authy.com/what-is-2fa/
[8]: https://fastmail.blog/2016/07/22/how-totp-authenticator-apps-work/