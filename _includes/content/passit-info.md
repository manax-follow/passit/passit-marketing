
## Features

- Store passwords and secure notes
- Access Passit across platforms and browsers
- Read passwords while offline
- Install browser extensions
- Import and export CSV files with passwords to get data into and out of Passit
- Create groups to organize passwords
- Add other users to groups on a self-hosted instance to share passwords

## About Passit

Passit was created by [Burke Software and Consulting](http://burkesoftware.com), a small team of software developers doing consulting work and supporting open source software development. Passit is self-funded from our consulting business for now, though we are certainly open to raising additional funds.

Passit is open source, which means that you can get involved in the project as well if you want! If you're interested in doing so, visit our [Contributing](/contributing/) page for more information.
