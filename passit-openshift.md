---
layout: page
title: "Passit Installation Guide: OpenShift YAML Template"
---

[Return to Passit installation guide](/install/)

```
---
kind: Template
apiVersion: v1
metadata:
  name: passit
  annotations:
    openshift.io/display-name: Passit PostgreSQL
    description: Passit - the open source Password Manager
    openshift.io/provider-display-name: Burke Software and Consulting
    openshift.io/documentation-url: https://passit.io
    template.openshift.io/bindable: 'false'
message: |-
  The following service(s) have been created in your project: ${NAME}.
labels:
  template: passit
  app: passit
objects:
- kind: Secret
  apiVersion: v1
  metadata:
    name: "${NAME}"
  stringData:
    database-user: "${DATABASE_USER}"
    django-secret-key: "${DJANGO_SECRET_KEY}"
    database-password: "${DATABASE_PASSWORD}"
    email-url: "${EMAIL_URL}"
- kind: Service
  apiVersion: v1
  metadata:
    name: "${NAME}"
    annotations:
      description: Passit django backend + Angular frontend
      service.alpha.openshift.io/dependencies: '[{"name": "${DATABASE_SERVICE_NAME}",
        "kind": "Service"}]'
  spec:
    ports:
    - name: web
      port: 8080
      targetPort: 8080
    selector:
      name: "${NAME}"
- kind: Route
  apiVersion: v1
  metadata:
    name: "${NAME}"
  spec:
    host: "${APPLICATION_DOMAIN}"
    to:
      kind: Service
      name: "${NAME}"
    tls:
      termination: edge
- kind: ImageStream
  apiVersion: v1
  metadata:
    name: "${NAME}"
    annotations:
      description: Stable Passit docker image 
  spec:
    tags:
    - from:
        kind: DockerImage
        name: "passit/passit:stable"
      name: latest
- kind: DeploymentConfig
  apiVersion: v1
  metadata:
    name: "${NAME}"
    annotations:
      description: Defines how to deploy the passit docker image
      template.alpha.openshift.io/wait-for-ready: 'true'
  spec:
    strategy:
      type: Rolling
      rollingParams:
        intervalSeconds: 1
        maxSurge: 0%
        maxUnavailable: 100%
        pre:
          execNewPod:
            command:
            - ./manage.py
            - migrate
            - --noinput
            containerName: ${NAME}
            env:
            - name: PYTHONUNBUFFERED
              value: "1"
          failurePolicy: Abort
        timeoutSeconds: 600
        updatePeriodSeconds: 1
    triggers:
    - type: ImageChange
      imageChangeParams:
        automatic: true
        containerNames:
        - passit
        from:
          kind: ImageStreamTag
          name: "${NAME}:latest"
    - type: ConfigChange
    replicas: 1
    selector:
      name: "${NAME}"
    template:
      metadata:
        name: "${NAME}"
        labels:
          name: "${NAME}"
      spec:
        containers:
        - name: passit
          image: " "
          ports:
          - containerPort: 8080
          readinessProbe:
            timeoutSeconds: 3
            initialDelaySeconds: 3
            httpGet:
              path: "/api/ping/"
              port: 8080
          livenessProbe:
            timeoutSeconds: 3
            initialDelaySeconds: 30
            httpGet:
              path: "/api/ping/"
              port: 8080
          env:
          - name: DATABASE_SERVICE_NAME
            value: "${DATABASE_SERVICE_NAME}"
          - name: DATABASE_NAME
            value: "${DATABASE_NAME}"
          - name: DATABASE_USER
            valueFrom:
              secretKeyRef:
                name: "${NAME}"
                key: database-user
          - name: DATABASE_PASSWORD
            valueFrom:
              secretKeyRef:
                name: "${NAME}"
                key: database-password
          - name: APP_CONFIG
            value: "${APP_CONFIG}"
          - name: SECRET_KEY
            valueFrom:
              secretKeyRef:
                name: "${NAME}"
                key: django-secret-key
          - name: IS_DEBUG
            value: 'False'
          - name: EMAIL_URL
            valueFrom:
              secretKeyRef:
                name: "${NAME}"
                key: email-url
          - name: FROM_EMAIL
            value: ${FROM_EMAIL}
          - name: EMAIL_CONFIRMATION_HOST
            value: '${APPLICATION_DOMAIN}'
          - name: IS_PRIVATE_ORG_MODE
            value: '${IS_PRIVATE_ORG_MODE}'
          - name: ENVIRONMENT
            value: production 
          resources:
            limits:
              memory: "${MEMORY_LIMIT}"
parameters:
- name: NAME
  displayName: Name
  description: The name assigned to all of the frontend objects defined in this template.
  required: true
  value: passit
- name: NAMESPACE
  displayName: Namespace
  required: true
  description: The OpenShift Namespace where the ImageStream resides.
  value: openshift
- name: MEMORY_LIMIT
  displayName: Memory Limit
  required: true
  description: Maximum amount of memory the Passit django container can use.
  value: 512Mi
- name: APPLICATION_DOMAIN
  displayName: Application Hostname
  description: The exposed hostname that will route to the Django service, if left
    blank a value will be defaulted.
  value: ''
- name: DATABASE_SERVICE_NAME
  displayName: Database Service Name
  required: true
  value: postgresql
- name: DATABASE_NAME
  displayName: Database Name
  required: true
  value: default
- name: DATABASE_USER
  displayName: Database Username
  required: true
  value: django
- name: DATABASE_PASSWORD
  displayName: Database User Password
  required: true
- name: DJANGO_SECRET_KEY
  displayName: Django Secret Key
  description: Set this to a long random string.
  generate: expression
  from: "[\\w]{50}"
- name: EMAIL_URL
  displayName: EMAIL SMTP authentication URL. Defaults only to console mail (does not send email). Example smtp://postmaster@passit.io:password@smtp.example.com
  value: "consolemail://"
- name: IS_PRIVATE_ORG_MODE 
  displayName: Is this a private server where all user contacts should be shared with other users? Such as a company's internal passit server. If you don't know it's fine to set to false.
  required: true
  value: "False"
```
