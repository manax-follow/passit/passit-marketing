# Passit Marketing

## Install and Run with Docker

- `docker-compose up` to serve the jekyll site on http://localhost:1314 and run `gulp watch`
- `gulp watch` only looks at the sass though. If you happen to be screwing with JS, open another terminal and run `docker-compose run --rm gulp gulp copy` after a change. I believe this is because I was having issues where copy didn't always copy everything and caused unexpected errors, coupled with the fact that we don't have a ton of JS in this project.
- Yeah I know, it's cumbersome. You can open the gulpfile and change the `watch` task to `default` and see what happens.

## Install without Docker

- `bundle install` or `gem install jekyll`
- `npm install`

## Run without Docker

- `npm run jekyll` to serve the site on http://localhost:1314
- `npm run build` to build the `dist` folder, then `npm run watch` to watch Sass directory

The biggest issue with running outside of Docker is that maybe you'll be running another jekyll blog with different gem versions. Have to keep everything on one version or accept that things could get different/weird.