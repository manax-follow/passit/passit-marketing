---
layout: page
title: "Hosted Passit Architecture Overview"
---

The intention of this page is to inform users of hosted Passit, how it's deployed, and help with compliance needs.

Burke Software does not share user data with any third parties. We do rely on DigitalOcean for hosting and Cloudflare for our CDN.

![](/images/passit-saas.png)

## Platform Architecture and Transparency

- User data is stored in Postgres using managed DigitalOcean Postgres service.
  - Our database cluster is only available within our DigitalOcean Kubernetes cluster's "Trusted Source". It is not internet accessible.
  - Authentication requires SSL and is stored in Kubernetes Secrets. Authentication information is not kept in any git repository.
- Our web servers are run in Kubernetes using managed DigitalOcean Kubernetes.
- Analytics are stored in Matomo and hosted on a private DigitalOcean Ubuntu server.
- Emails are sent with Mailgun.
- Web traffic passes through Cloudflare CDN.
- This marketing page is served via GitLab Pages.
- Subscriptions and payments are handled by Stripe. Burke Software does not store credit card information. Only subscription related information is sent to Stripe. Error data is not shared.
- Docker images used for both hosted Passit and self-hosting are hosted on both [GitLab](https://gitlab.com/passit/passit-frontend/container_registry) and [Docker Hub](https://hub.docker.com/r/passit/passit). These images are built in [GitLab CI](https://gitlab.com/passit/passit-frontend/-/pipelines).
- Cookies are used for session based authentication when using Django Admin and MFA functionality. Cookies are never shared with third parties. Local Storage is used to save authentication tokens when selecting “stay logged in on this device”.
- Users who wish to purge all account information should email [info@burkesoftware.com](mailto:info@burkesoftware.com).
- Mozilla Observatory rates app.passit.io as "B+". View [report](https://observatory.mozilla.org/analyze/app.passit.io). To keep users safe, we utilize Content Security Policy, secure cookies, HTTPS, and HSTS.

## Disaster Recovery

Hosted Passit relies on DigitalOcean’s [Managed Kubernetes](https://www.digitalocean.com/docs/kubernetes/) and [Managed PostgreSQL](https://www.digitalocean.com/docs/databases/postgresql/) for a robust, fault tolerant platform. DigitalOcean publishes their Droplet Policies [here](https://www.digitalocean.com/docs/droplets/resources/policies/#droplet-service-level-agreement-sla). Individual services including Kubernetes Nodes, Kubernetes Pods, and Database Clusters automatically restore themselves.

All hosted Passit assets are deployed to DigitalOcean’s NYC1 region. Hosted Passit is susceptible to region wide availability issues. DigitalOcean publishes status [here](https://status.digitalocean.com/).

### Recovery Time Objective

In the majority of cases, recovery time from underlying infrastructure is determined by DigitalOcean. In cases where Passit itself has a service interruption, our recovery time objective is within 1 hour during EST business hours and is best effort outside of business hours. 

### Recovery Point Objective

Hosted Passit data is saved in DigitalOcean’s Managed PostgreSQL and is backed up once per day and retained for 7 days.

Hosted Passit infrastructure is configured using Terraform and is not publicly available. All changes to infrastructure are managed in gitlab.com where history is saved indefinitely.


## Process controls

- Burke Software employees are required to utilize Single Sign On via Google Apps and Two-Factor authentication when accessing privileged hosting services including DigitalOcean and GitLab version control systems.
- Authorization for hosting services is provisioned via Terraform in a private git repository. All permission requests are logged via git commits.

Need more information? Email us at [info@burkesoftware.com](mailto:info@burkesoftware.com)

Found a security vulnerability? [Open a private issue on GitLab](https://gitlab.com/passit).
