---
layout: page
title: "Passit Installation Guide"
---

If you are looking to evaluate using Passit, the fastest way is our [hosted version]({{site.web_app_url}}). If you are looking to develop Passit, read our [Gitlab README](https://gitlab.com/passit/passit-frontend).

To host Passit yourself, you need:

- A PostgreSQL database.
- A Linux server or platform as a service that supports Docker
- An SMTP server - for personal use, Gmail is fine.
- A domain with SSL/TLS. [Let's Encrypt](https://letsencrypt.org/) works great.

We suggest using either Docker Compose, DigitalOcean App Platform, Openshift Online, or Heroku.

*To learn more about updating Passit, visit our [update guide](/update/).*

## Run on any Linux server with Docker

This method assumes you know how to set up DNS records, install Linux packages, and configure nginx.
If you prefer a "it just works" solution we recommend the [hosted version]({{site.web_app_url}}) or Heroku.

1. Add DNS record pointing to machine and ensure ports 80 and 443 are open. For example passit.yourcompany.com
2. Install Docker and Docker Compose.
3. Install PostgreSQL locally or use a service like Amazon RDS.
4. Copy [docker-compose.production.yml](https://gitlab.com/passit/passit-frontend/blob/master/docker-compose.production.yml) to your server.
5. Edit the docker-compose.production.yml and optionally edit the environmental variables listed in it, documented below under "Configurations". You must at least set the database url and SMTP server.
6. Run `docker-compose up -d` to start Passit.
7. Install nginx or your favorite web server or load balancer.
8. Configure virtual hosts in nginx. Use proxy_pass to forward requests to the Docker based service.
9. Run the django migrations (do this on every upgrade). In the container run `./manage.py migrate`. If using docker-compose, you can run `docker-compose run --rm web ./manage.py migrate`.
10. Install [certbot](https://certbot.eff.org/) to set up SSL. This is required.

Example nginx configuration in `/etc/nginx/sites-enabled/default` BEFORE running certbot.

```
server { 
        server_name mydomain.com; 
 
        location / { 
          proxy_pass http://127.0.0.1:8000; 
          proxy_set_header Host $host; 
        } 
} 
```

Finally just go to mydomain.com and you should see the frontend.

## With Digital Ocean App Platform

Passit on DO App Platform consists of a web, migration job, and postgres database. Start by clicking the following button. Note this link acts as a referral and helps fund Passit.

<a style="box-shadow: unset; transition: unset" href="https://cloud.digitalocean.com/apps/new?repo=https://gitlab.com/passit/passit/tree/master&refcode=7e90b8fb37f8" target="_blank">![Deploy to DO](https://www.deploytodo.com/do-btn-blue.svg)</a>

Leave environment variables blank and click next. Pick the basic or pro plan. One 512 MB RAM &#124; 1 vCPU is fine to start with. Click Launch. Now copy [app-platform.yaml](https://gitlab.com/passit/passit/-/blob/master/app-platform.yaml) from the [GitLab Passit Repo](https://gitlab.com/passit/passit/) to your local computer. Edit the following

### Environment Variables

The environment variables you will need to set can be found in the [Required settings](https://passit.io/install/#required-settings) section.

### Name and region

This can be anything. We default to "passit" and "nyc".

### Deploying

You'll need to install [doctl](https://www.digitalocean.com/docs/apis-clis/doctl/how-to/install/) and log in. 

Run `doctl apps list` to get your app's id.

Now apply your app-platform.yaml spec with `doctl apps update 11111111-1111-1111-1111-111111111 --spec app-platform.yaml` (enter your actual id)

After deployment, you should be able to visit the app URL and start using Passit!

### Production considerations

If you intend to use Passit in production, consider upgrading your Postgres database to a production instance. In the web interface, go to Manage Components, passit-db, Upgrade to a managed database.

If you haven't already, you'll need to set up email via environment variables. 

### Upgrading Passit

By default, the docker image tag is "latest". Click Deploy to upgrade to the latest Passit docker image.

## With OpenShift

OpenShift v3 is a great way to run Passit. These instructions are a good starting point for Kubernetes as well. A nice advantage of using OpenShift is that database migrations can run automatically.

![](/images/openshift_dashboard.png)

1. Create a Postgres database either in OpenShift itself or a service like RDS.
2. Enable the hstore postgresl extension. From a OpenShift postgresql pod bash terminal it would look like: `sql sampledb -c "create extension hstore;`
3. Import [this YAML template](/passit-openshift/) under "Add to project". Review each parameter and make sure to enter your database connection information.
4. Configure SSL. With OpenShift online you could use the provided openshiftapps.com domain or set your own. To use openshiftapps.com simply autogenerate the hostname (leave it blank). To use your own domain, we suggest at this time purchasing a cert as openshift is difficult to use with let's encrypt. To install the cert just edit the route made in step 3.

## With Heroku

Heroku is supported via the Docker container runtime. At this time database migrations must be run manually. We show Heroku CLI shortcuts, but steps 1, 2, and 4 can be done on [Heroku Dashboard](https://dashboard.heroku.com) if you're more comfortable there.

1. Create a new Heroku app.
    - `heroku apps:create <heroku-app-name>`
2. Add a Postgres database to your new app.
    - `heroku addons:create heroku-postgresql --app <heroku-app-name>`
3. Deploy the Docker image.
    - `docker pull {{site.docker_image}}`
    - `docker tag {{site.docker_image}} registry.heroku.com/<heroku-app-name>/web`
    - `heroku container:login`
    - `docker push registry.heroku.com/<heroku-app-name>/web`
    - `heroku container:release web`
4. Set environment variables.
    - `DATABASE_URL` should already be set
    - `IS_DEBUG` should be False
    - `SECRET_KEY` should be a random string of 40 characters.
    - `heroku config:set ENV_VAR=Value --app  <heroku-app-name>`
    - `heroku config --app  <heroku-app-name>` to confirm that everything is set
    - View the Configuration section below for more information and variables.
5. Manually migrate the database: `heroku run --app <heroku-app-name> ./manage.py migrate`

The app should be up and running, but you'll need to configure email in order to complete account registrations.

## Without Docker

This method is not supported. Only do this if you do not wish to use Docker and know how to run a basic Django app with static HTML front end.

Checkout the master branch of [passit-backend](https://gitlab.com/passit/passit-backend) Run Django as you would any Django app with something like uWSGI + nginx. 

Download the static [passit-frontend](https://gitlab.com/passit/passit-frontend/pipelines) build (click the download arrow and then Download build_yarn artifacts). Place the files where you serve static html/css/js files from.

passit.yourdomain.com  < Static html
api.passit.yourdomain.com  < Django server

Set up the environmental variables below.
Set up SSL on both domains.

# Configuration

Configuration is done by setting these environment variables

## Required settings

  - `DATABASE_URL`: PostgreSQL URL as parsed by [dj-database-url](https://github.com/kennethreitz/dj-database-url)
  - `EMAIL_URL`: The SMTP url to use for sending email - see email_url from [here](http://django-environ.readthedocs.io/en/latest/#supported-types) for syntax. For Gmail it would be like `smtp+tls://user@:password@smtp.gmail.com:587`
  - `DEFAULT_FROM_EMAIL`: Email to use as sender
  - `EMAIL_CONFIRMATION_HOST`: Host that should used in confirmation email to link back to where to confirm.
  - `SECRET_KEY`: Make this unique but it can be anything. 40+ characters is good. This is the Django server `SECRET_KEY`. Do not share this.
 
## Optional settings

  - `IS_PRIVATE_ORG_MODE`: (default: False) Set True for a private Passit instance in which all contacts are shared (no need to look up by exact email).
  - `PRIVATE_ORG_MODE_DOMAIN_WHITELIST`: (default: None) Comma seperated domain whitelist for self registration. Example `mycompany.com,otherplace.org`.

# Extensions

- [Install Passit for Firefox]({{ site.firefox_extension_url }})
- [Install Passit for Chrome]({{ site.chrome_extension_url }})
